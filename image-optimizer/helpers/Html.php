<?php

namespace app\helpers;

/**
 * Html provides a set of static methods for generating commonly used HTML tags.
 */
class Html extends \yii\helpers\BaseHtml {

    public static function addVersion($str, $host = NULL) {
        static $v = NULL;
        static $h = NULL;
        if (NULL === $v) {
            $v = trim ( file_get_contents ( $_SERVER['DOCUMENT_ROOT'] . '/version.txt' ) );
        }
        if (NULL === $host) {
            $h = \Yii::$app->params['cdn']['staticHost'];
        }
        return $h . $str . '?v=' . $v;
    }

    public static function addCdn($str, $host = NULL) {
        static $v = NULL;
        static $h = NULL;
        if (NULL === $v) {
            $v = trim ( file_get_contents ( $_SERVER['DOCUMENT_ROOT'] . '/version.txt' ) );
        }
        if (NULL === $host) {
            $h = \Yii::$app->params['cdn']['cdnHost'];
        }
        return $h . $str . '?v=' . $v;
    }

    public static function staticImage($str, $host = NULL) {
        static $v = NULL;
        static $h = NULL;
        if (NULL === $v) {
            $v = trim ( file_get_contents ( $_SERVER['DOCUMENT_ROOT'] . '/version.txt' ) );
        }
        if (NULL === $host) {
            $h = \Yii::$app->params['cdn']['imgHost'];
        }
        return $h . $str . '?v=' . $v;
    }
}
