<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register ( $this );
// $this->beginPage ();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?= Yii::$app->language ?>">
<!--<![endif]-->
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <?= Html::csrfMetaTags()?>
        <title><?= Html::encode($this->title) ?></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" href="apple-touch-icon.png">

<link rel="stylesheet" href="css/normalize.min.css">
<link rel="stylesheet" href="css/main.css">

<!-- <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script> -->
        <?php $this->head()?>

    </head>
<body ng-app="djpImageComparatorApp">
	<!--[if lt IE 10]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<div class="header-container">
		<header class="wrapper clearfix">
			<h1 class="title">Compare Uncompressed Images</h1>
		</header>
	</div>

	<div class="main-container">

		<div class="main wrapper clearfix">
            <?= $content?>
            </div>
		<!-- #main -->
	</div>
	<!-- #main-container -->

	<div class="footer-container">
		<footer class="wrapper">
			<h3>Author: Daniel J. Post</h3>
			<h3>Phone: 612-367-6902</h3>
			<h3>Email: geek@danieljpost.info</h3>
		</footer>
	</div>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
	<script
		src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.js"></script>
	<script
		src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-route.js"></script>
	<script
		src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-sanitize.js"></script>
	<!--         <script src="//ajax.googleapis.com/ajax/libs/angular_material/0.8.3/angular-material.min.js"></script> -->
<!-- 	<script src="/js/beforeafter/js/jquery.beforeafter-1.4.js"></script> -->
	<script src="/js/app.js"></script>
<!-- 	<script src="/js/controllers.js"></script> -->
        <?php  // $this->endBody()?>
    </body>
</html>

