<?php
/* @var $this yii\web\View */
$this->title = 'Image Comparator';
?>
<script>
        var imageList = <?php echo $imageList;?>;
</script>

<div class="site-index" ng-controller="djpImagesController">

	<section
		style="position: fixed; left: 0; top: 0; background-color: white; padding: 10px; outline: 1px solid black; box-shadow: 1px 1px 1px #000">
		<header>
			<h3>
				Your current parameters match<br /> {{(imageList |
				filter:selected).length}} of {{imageList.length}} images.
			</h3>
		</header>

		<fieldset>
			<label for="compressionThreshold">Ratio</label><br />
			<input type="range" min="0" max="1" step=".05"
				ng-model="compressionThreshold" value="{{compressionThreshold}}">{{compressionThreshold}}
		</fieldset>

		<fieldset>
			<label for="bytesSavedThreshold">Bytes Saved Threshold</label><br />
			<input id="bytesSavedThreshold" type="range" min="10000" max="100000"
				ng-model="bytesSavedThreshold" step="10000"
				value="{{bytesSavedThreshold}}">{{bytesSavedThreshold}}
		</fieldset>

		<fieldset>
			<header ng-if="(imageList|filter:selected).length > 200">
				<h2>If you show images now, you might crash your browser or make
					your computer cry</h2>
			</header>
			<header>Show images now?</header>
			<input id="showImagesNow" type="radio" ng-model="showImagesNow"
				value="Yes">&nbsp;<label for="showImagesNowTrue">Yes</label> <input
				id="showImagesNow" type="radio" ng-model="showImagesNow" value="No">&nbsp;<label
				for="showImagesNowFalse">No</label>
			<div>
				<b>{{showImagesNow}}</b>
			</div>
		</fieldset>
	</section>

	<section class="imagessses">
		<section
			ng-repeat="image in imageList | filter:selected | orderBy: image.original.imageInfo.size :reverse"
			ng-display="'show'">
			<header>{{image.original.id}}</header>
			<section>
				<p>Original size: {{image.original.imageInfo.size}}</p>
				<p>Optimized size: {{image.optimized.imageInfo.size}}</p>
				<p>Bytes Saved: {{image.sizeDifference}} bytes</p>
				<p>Ratio: {{image.ratio*100|number : 2}}%</p>
			</section>
			<section ng-if="showImagesNow == 'Yes'">
				<div style="width: 100%; overflow: scroll;">
					<img style="" src="/image-original/?img={{image.original.id}}"> <img
						style="" src="/image-optimized/?img={{image.optimized.id}}">
					<button></button>
				</div>
				<br style="clear: both">
			</section>
			<br style="clear: both">
		</section>
	</section>

</div>
