<?php
$localParams['staticHost'] = '/';
// URL to host static assets (e.g. Angular JS)
$localParams['cdnHost'] = 'https://code.angularjs.org/1.2.9/';
$localParams['imgHost'] = '/img/'; // hosted locally

return [
    'adminEmail' => 'geek@danieljpost.info',
    'cdn' => [
        'staticHost' => $localParams['staticHost'], 'imgHost' => $localParams['imgHost'],
        'cdnHost' => $localParams['cdnHost']
    ],
    'imageConfig' => [
        'backupPath' => 'C:\code\REVContent\revbackup', 'basePath' => 'C:\code\REVContent\rev', 'workingPath' => 'C:\code\REVContent\revworking'
    ]
];
