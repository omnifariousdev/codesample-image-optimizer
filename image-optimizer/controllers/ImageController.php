<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models;
use yii\web\JsonResponseFormatter;

class ImageController extends Controller {

    public function behaviors() {
        return [
        /*
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        */
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction', 'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ]
        ];
    }

    public function actionImageInfo($img) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return models\Image::find ( $img );
    }

    public function actionImageOptimized($img) {
        $imgModel = models\Image::findOptimized ( $img );
        return \Yii::$app->response->sendFile ( $imgModel->realpath );
    }

    public function actionImageOriginal($img) {
        $imgModel = models\Image::findOriginal ( $img );
        return \Yii::$app->response->sendFile ( $imgModel->realpath );
    }

    public function actionIndex() {
        return $this->render('index', ['imageList' => models\Image::findAll()]);
    }

    public function actionListImages() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return models\Image::findAll();
    }
}
