<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Image;
use app\models\ImageInfo;

class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className (), 'only' => [
                    'logout'
                ],
                'rules' => [
                    [
                        'actions' => [
                            'logout'
                        ], 'allow' => true, 'roles' => [
                            '@'
                        ]
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className (),
                'actions' => [
                    'logout' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction', 'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ]
        ];
    }

    public function actionIndex() {
        $imageArray = Image::findAll ();
        $imageList = [ ];
        $i = 0;
//         $limit = 50;
        $limit = 50000;
        foreach ( $imageArray as $key => $filename ) {
            if (preg_match('/\.(png|gif)/', $filename)) { continue; }
            $img = Image::find ( $filename );
            $imageList[$key] = json_encode ( $img );
            if ($i++ > $limit) {
                break;
            }
        }
        $imageString = '[' . implode ( ',', $imageList ) . ']';
        return $this->render ( 'index', [
            'imageList' => $imageString
        ] );
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome ();
        }

        $model = new LoginForm ();
        if ($model->load ( Yii::$app->request->post () ) && $model->login ()) {
            return $this->goBack ();
        } else {
            return $this->render ( 'login', [
                'model' => $model
            ] );
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout ();

        return $this->goHome ();
    }

    public function actionContact() {
        $model = new ContactForm ();
        if ($model->load ( Yii::$app->request->post () ) && $model->contact ( Yii::$app->params['adminEmail'] )) {
            Yii::$app->session->setFlash ( 'contactFormSubmitted' );

            return $this->refresh ();
        } else {
            return $this->render ( 'contact', [
                'model' => $model
            ] );
        }
    }

    public function actionAbout() {
        return $this->render ( 'about' );
    }
}
