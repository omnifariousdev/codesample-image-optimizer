'use strict';

/* Controllers */
var globalVar;
var djpImageComparatorApp = angular.module('djpImageComparatorApp',[]);
var i = 0;
djpImageComparatorApp.controller('djpImagesController', ['$scope', function($scope) {
    $scope.selected = function(item) {
        return item.ratio < $scope.compressionThreshold || item.sizeDifference > $scope.bytesSavedThreshold;// ('selected' == item.status);
    };
    $scope.i = 0;
    $scope.compressionThreshold = .2;
    $scope.bytesSavedThreshold = 100000;
    $scope.showImagesNow = "No";

    // from global scope.
    $scope.imageList = imageList;

    // never do the following in production, it makes hacking too easy.
    globalVar = $scope;
}]);
