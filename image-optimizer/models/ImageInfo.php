<?php

namespace app\models;

use yii\base\Model;

class ImageInfo extends Model {
    const STATUS_UNOPTIMIZED = 0;
    const STATUS_BACKED_UP = 1;
    const STATUS_OPTIMIZED = 4;
    const STATUS_UPLOADED = 4;

    public $filename;
    public $dimensions = [
        'height' => 0, 'width' => 0
    ];
    public $exifData = [];

    public static function getInfo($filename) {
        $info = new \stdClass();
        try {
        if (file_exists ( $filename )) {
            $info->dimensions = getimagesize ( $filename );
            if (FALSE !== stripos('.png', $filename)) {
                $info->exifData = exif_read_data ($filename);
            }
        }
        } catch (\Exception $e) {
            \Yii::error('Exception reading file '. $filename);
        }

        return $info;
    }

}

