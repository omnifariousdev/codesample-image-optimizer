<?php

namespace app\models;

use yii\base\Model;

class Image extends Model {
    const STATUS_UNOPTIMIZED = 0;
    const STATUS_BACKED_UP = 1;
    const STATUS_OPTIMIZED = 4;
    const STATUS_UPLOADED = 4;

    public $id;
    public $imageInfo;
    public $path;
    public $optimizedStatus = self::STATUS_UNOPTIMIZED;
    private static $_configuration = NULL;

    public function __construct($config = []) {
        if (!isset ($config['which']));
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    public function getConfiguration() {
        if (NULL === static::$_configuration) {
            // TODO: load from Yii::$app->configurations
            static::$_configuration = \Yii::$app->params['imageConfig'];
        }
        return static::$_configuration;
    }

    public static function find($filename) {
        $optimized = static::findOptimized ( $filename );
        $original = static::findOriginal ( $filename );
        $return = [
            'optimized' => $optimized,
            'original' => $original,
            'sizeDifference' => 0,
            'ratio' => 1
        ];
        if (isset($optimized->imageInfo) && $original) {
            $return['sizeDifference'] = $original->imageInfo->size - $optimized->imageInfo->size;
            $return['ratio'] = $optimized->imageInfo->size / $original->imageInfo->size;
        }
        return $return;
    }

    private static function getInstance() {
        static $instance = NULL;
        if (NULL === $instance) {
            $instance = new self ();
        }
        return $instance;
    }

    public static function findImage($filename, $path = 'workingPath') {
        $what = static::getInstance ()->configuration[$path] . DIRECTORY_SEPARATOR . $filename;
        \Yii::trace ( $what );
        if (file_exists ( $what )) {
            $result = new \SplFileInfo ( $what );
            $return = new self ();
            $return->id = $filename;
            $return->path = static::getInstance ()->configuration[$path];
            $return->imageInfo = ImageInfo::getInfo($what);
            $return->imageInfo->size = $result->getSize ();
            $return->optimizedStatus = Image::STATUS_OPTIMIZED;
            return $return;
        } else {
            return $what;
        }
    }


    public static function findAll($path = 'basePath') {
        $wd = static::getInstance ()->configuration[$path];
        // find all the files in $path directory matching the specified regex.
        $flags = \FilesystemIterator::UNIX_PATHS | \FilesystemIterator::KEY_AS_PATHNAME |
        \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS;
        $directory = new \RecursiveDirectoryIterator ( $wd, $flags );
        $filter = new \RecursiveCallbackFilterIterator ( $directory,
                function ($current, $key, $iterator) {
                    // Skip hidden files and directories, e.g. .git
                    if ($current->getFilename ()[0] === '.') {
                        return FALSE;
                    }
                    return $current->isDir () || preg_match ( '/\.(jpg|gif|png)/', $current->getFilename () );
                } );
                $iterator = new \RecursiveIteratorIterator ( $filter );
                $files = [ ];
                $search =str_replace(DIRECTORY_SEPARATOR, '/', static::getInstance ()->configuration[$path]);
                $iterationLimit = 100;
                foreach ( $iterator as $i => $info ) {
                    try {
                        json_encode($info->getPathname());
                    } catch (\Exception $e) {
                        \Yii::error('could not json_encode the path name: '. $info->getPathname());
                        continue;
                    }
                    $files[] = str_replace($search,'',str_replace(DIRECTORY_SEPARATOR, '/', $info->getPathname ()));
                    if ($i>$iterationLimit) break;
                }
                return $files;

    }


    public static function findOptimized($filename) {
        return static::findImage($filename, 'workingPath');
    }

    public static function findOriginal($filename) {
        return static::findImage($filename, 'basePath');
    }

    public static function findBackup($filename) {
        return static::findImage($filename, 'backupPath');
    }

    public function getRealpath() {
        $filename = $this->path . DIRECTORY_SEPARATOR . $this->id;
        return file_exists($filename)?$filename:\Yii::$app->params['notfoundImage'];
    }
}

